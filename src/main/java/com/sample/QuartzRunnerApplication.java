package com.sample;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class QuartzRunnerApplication {

	static String[] springConfig = { "spring-batch-config.xml", "spring-batch-csv.xml", "spring-quartz-database.xml" };

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
		System.out.println("Done");
	}
}
