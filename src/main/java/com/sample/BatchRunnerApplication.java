package com.sample;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BatchRunnerApplication {

	static String[] springConfig = { "spring-batch-config.xml" , "spring-batch-csv.xml" };

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);

		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job job = (Job) context.getBean("csvJob");

		try {
			//JobParameters param = new JobParameters();
			//param.getParameters().put("Date", new JobParameter(new java.util.Date()));
			// JobExecution execution = jobLauncher.run(job, param);
			JobExecution execution = jobLauncher.run(job,
					new JobParametersBuilder().addLong("timestamp", System.currentTimeMillis()).toJobParameters());

			System.out.println("Exit Status : " + execution.getStatus());

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Done");
	}
}
