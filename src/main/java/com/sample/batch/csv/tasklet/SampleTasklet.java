package com.sample.batch.csv.tasklet;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class SampleTasklet implements Tasklet {

	private List<String> myCol = null;
	
	
	public SampleTasklet() {
		myCol = new ArrayList<>();
		
		myCol.add("S");
		myCol.add("A");
		myCol.add("S");
		myCol.add("M");
		myCol.add("I");
		myCol.add("T");
		
	}
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		myCol.forEach(System.out::println);
		
		return RepeatStatus.FINISHED;
	}

}
