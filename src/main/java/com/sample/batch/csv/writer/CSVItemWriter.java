package com.sample.batch.csv.writer;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

import com.sample.model.CSVVO;

public class CSVItemWriter implements ItemWriter<CSVVO> {

	@Override
	public void write(List<? extends CSVVO> items) throws Exception {
		
		items.forEach(System.out::println);
	}

}
