package com.sample.batch.csv.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.sample.model.CSVVO;

public class CSVItemReader implements ItemReader<CSVVO> {
	
	List<CSVVO> csvRows = null;
	int counter;
	String inputFile;
	
	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}
	
	
	@BeforeStep
	public void initialize() {
		csvRows = processInputFile(this.inputFile).stream().filter(x -> x!=null).collect(Collectors.toList());;
		counter = 0;
		
	}
	
	
	@AfterStep
	public void destroy() {
		
		/*
		try {
			Thread.sleep(240000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		*/
		System.out.println(".............Batch completed at " + new java.util.Date());
	}

	@Override
	public CSVVO read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		
		
		if(counter < csvRows.size()) {
			return csvRows.get(counter++);
		}else {
			return null;
		}
	}
	
	
	
	private List<CSVVO> processInputFile(String inputFilePath){
		List<CSVVO> tempCsvRows = null;
		
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(inputFilePath))))) {
			tempCsvRows = br.lines().map(mapToObject).collect(Collectors.toList());
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return tempCsvRows;
	}
	
	private Function<String,CSVVO>  mapToObject = (row) -> {
		String[] split = row.split(",");
		CSVVO temp = null;
		if(split.length >= 9 ) {
			temp = new CSVVO(row.split(","));
		}
		return  temp;
	};
	
	/*public static void main(String[] args) {
		InMemoryItemReader i = new InMemoryItemReader();
		List<CSVVO> rows = i.processInputFile("C:\\workspaces\\inputs\\SampleCSVFile_10600kb.csv");
		
		rows.stream().filter(x -> x!=null).forEach(System.out::println);
	}*/

}
