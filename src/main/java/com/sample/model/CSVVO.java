package com.sample.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CSVVO {
	private String cell1;
	private String cell2;
	private String cell3;
	private String cell4;
	private String cell5;
	private String cell6;
	private String cell7;
	private String cell8;
	private String cell9;

	public CSVVO(String cell1, String cell2, String cell3, String cell4, String cell5, String cell6, String cell7,
			String cell8, String cell9) {
		
		this.cell1 = cell1;
		this.cell2 = cell2;
		this.cell3 = cell3;
		this.cell4 = cell4;
		this.cell5 = cell5;
		this.cell6 = cell6;
		this.cell7 = cell7;
		this.cell8 = cell8;
		this.cell9 = cell9;
	}

	public CSVVO(String[] split) {
		this.cell1 = split[0];
		this.cell2 = split[1];
		this.cell3 = split[2];
		this.cell4 = split[3];
		this.cell5 = split[4];
		this.cell6 = split[5];
		this.cell7 = split[6];
		this.cell8 = split[7];
		this.cell9 = split[8];
	}
/*
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
*/
	
	
	
	public String getCell1() {
		return cell1;
	}

	@Override
	public String toString() {
		return "CSVVO [cell1=" + cell1 + ", cell2=" + cell2 + ", cell3=" + cell3 + ", cell4=" + cell4 + ", cell5="
				+ cell5 + ", cell6=" + cell6 + ", cell7=" + cell7 + ", cell8=" + cell8 + ", cell9=" + cell9 + "]";
	}

	public String getCell2() {
		return cell2;
	}

	public String getCell3() {
		return cell3;
	}

	public String getCell4() {
		return cell4;
	}

	public String getCell5() {
		return cell5;
	}

	public String getCell6() {
		return cell6;
	}

	public String getCell7() {
		return cell7;
	}

	public String getCell8() {
		return cell8;
	}

	public String getCell9() {
		return cell9;
	}
	
	

}
